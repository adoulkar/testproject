require("dotenv").config();
const { dialog } = require("electron");
const fs = require("fs");
const portfinder = require("portfinder");
const child_process = require("child_process");

const SimpleNodeLogger = require("simple-node-logger"),
  opts = {
    logFilePath: "mylogfile.log",
    timestampFormat: "YYYY-MM-DD HH:mm:ss.SSS",
  },
  log = SimpleNodeLogger.createSimpleLogger(opts);
const runServer = (win) => {
  console.log("launching server");

  const options = {
    type: "error",
    buttons: ["No", "Yes"],
    title: "Question",
    message: `Go Server file is missing`,
    detail: "Do you want to continue?",
  };
  const cmd = "./push-start";

  try {
    if (fs.existsSync(cmd)) {
      const child = child_process.spawn(cmd);

      child.on("error", function (err) {
        console.log(data.toString("utf8"));
        log.error(data.toString("utf8"));
      });

      child.stdout.on("data", function (data) {
        console.log(data.toString("utf8"));
        log.info(data.toString("utf8"));
      });

      child.stderr.on("data", function (data) {
        console.log(data.toString("utf8"));
        log.error(data.toString("utf8"));
      });
    } else {
      displayPopup(options, win);
    }
  } catch (err) {
    console.log(err);
    console.log("in catch");

    displayPopup(options, win);
  }
};
const checkPort = async (win) => {
  console.log("checking port");
  const checkPort = parseInt(process.env.PORT || 8000);
  const options = {
    type: "error",
    buttons: ["OK"],
    title: "Question",
    message: `Port ${checkPort} is already is use`,
    detail: "Closing application",
  };
  const port = await portfinder.getPortPromise({
    checkPort,
  });
  console.log(port, checkPort);
  if (port !== checkPort) displayPopup(options, win);
  else runServer(win);
};

const displayPopup = async (options, win) => {
  const response = await dialog.showMessageBox(options);
  console.log(response.response);
  if (response.response == 0) {
    win.close();
  }
};

const checkServer = async function (win) {
  win = win;
  await checkPort(win);
};

module.exports = { checkServer };

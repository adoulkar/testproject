import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { AppConstants } from '../appConstants';
import { LoadingSpinnerService } from '../shared/loading/loading-spinner.service';
import { saveAs } from 'file-saver';
import * as $ from 'jquery';
import * as moment from 'moment';
import { WebsocketService } from './websocket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild(ToastContainerDirective, {static: true}) toastContainer: ToastContainerDirective;
  @ViewChild(ToastContainerDirective, {static: true}) errorToast: ToastContainerDirective;

  @ViewChild('videoSrc') videoSrc: ElementRef;

  selectedTab: any = 2;
  selectedNavPill: any = 1;
  maxDate: Date;
  currentDate: Date = new Date();
  // tomorrowsDate: Date;

  standardForm: FormGroup;
  stdToMinDate: Date = null;
  stdFromMaxDate: Date = null;
  valueChangesStdForms: Subscription;
  standardVideoDetails: any = [];
  noRecordsFoundFlag: Boolean = false;

  threeSixtyForm: FormGroup;
  tsToMinDate: Date = null;
  tsFromMaxDate: Date = null;
  valueChangesThreeSixtyForms: Subscription;
  tsVideoDetails: any = [];
  tsNoRecordsFoundFlag: Boolean = false;

  // File Progress
  fileProgressData: any = [];

  // Preload video seconds
  t: any = '#t=0.7';
  tCount: any = 1;

  // Profiles
  profiles: any = [];
  selectedProfile: any;

  constructor(private formBuilderInstance: FormBuilder, private toastrServiceInstance: ToastrService, private appServiceInstance: AppService, private loadingSpinnerInstance: LoadingSpinnerService, private websocketServiceInstance: WebsocketService, private elementRef: ElementRef, private renderer: Renderer2 ) { }

  ngOnInit(): void {
    this.maxDate = new Date();

// Web socket connection call due to 1st Tab is removed
    this.connectWebSocket();

// Filter validation
    // this.tomorrowsDate = this.getTomorrowsDate();
    // console.log("\n--tomorrowsDate--", this.tomorrowsDate);
    // this.maxDate = this.tomorrowsDate;
    this.standardFormValidations();
// By default minDate of To Date filed should be current date for date validation
    this.stdToMinDate = this.currentDate;

    // this.threeSixtyFormValidations();
    // this.showDownloadNotification();

    const seconds = 120;
    this.convertSecondsToMilliseconds(seconds);
    // this.errorMessageDisplay('occured while fetching details!');

// Progress of storing video
    // this.getFilesStatus();

    // setInterval(() => {
    //   this.getFilesStatus();
    // }, 120000);

    this.initializeProfiles();
  }

// Web socket connection for file progress tab
connectWebSocket() {
  const url = 'ws://localhost:8000/echo';
  this.websocketServiceInstance.webSocketConnection(url);

  this.websocketServiceInstance.myWebSocket.subscribe((data) => {
    // console.log("\n----data---myWebSocket--" + JSON.stringify(data));
    this.getFilesStatus(data);

  }, err => {
  this.errorMessageDisplay('Web Socket Connection Error!');
  console.log("\n----err---myWebSocket--" + JSON.stringify(err));

  });

}

// Generate tomorrows date for To Date filter option
getTomorrowsDate() {
  let nextDay = this.currentDate.getDate() + 1;
  let nextDayMonth = this.currentDate.getMonth();
  let nextDayYear = this.currentDate.getFullYear();
  let hours = this.currentDate.getHours();
  let minutes = this.currentDate.getMinutes();
  let second = this.currentDate.getSeconds();
  return new Date(nextDayYear, nextDayMonth, nextDay, hours, minutes, second);
}

// Default profile { , disabled: true }
initializeProfiles() {
  this.profiles = [];
  this.profiles = [
    {id: 1, name: 'Local installation'},
    {id: 2, name: 'NAS on client PC'},
    {id: 3, name: 'NAS on Server PC'},
    {id: 4, name: 'NAS on standalone'},
    // {id: 5, name: 'Klaipėda'}
  ];
}

// Seconds to Milliseconds
convertSecondsToMilliseconds(seconds) {
  const milliseconds = (seconds * 1000);
  // console.log("\n---milliseconds---", milliseconds);
  return milliseconds;
}
// File download notification - 'Files downloading...'
showDownloadNotification(message, channel) {
  // console.log("\n---message---", message);
  this.toastrServiceInstance.overlayContainer = this.toastContainer;
  this.toastrServiceInstance.success(message, channel,
  {
    closeButton: true,
    progressBar: true,
    progressAnimation: 'increasing',
    toastClass: 'ngx-toastr',
    positionClass: 'toast-top-right',
    tapToDismiss: false,
    timeOut: 40000,
  });
}
// Show error message
errorMessageDisplay(errorMessage) {
  this.toastrServiceInstance.overlayContainer = this.errorToast;
  this.toastrServiceInstance.error(errorMessage, 'Error',
  {
    toastClass: 'ngx-toastr',
    positionClass: 'toast-bottom-center',
    timeOut: 20000,
  });
}

showFileProgress() {
  this.selectedTab = 2;
}
// Form validation for From Date & To Date  moment(this.tomorrowsDate).format('DD/MM/YYYY')
standardFormValidations() {
  this.standardForm = this.formBuilderInstance.group({
    fromDate: [this.currentDate, Validators.required],
    toDate: [this.currentDate, Validators.required]
  });
  this.valueChangesStdForms = this.standardForm.valueChanges.subscribe((value) => {
    if (this.standardForm.get('fromDate').value) {
      this.stdToMinDate = this.standardForm.get('fromDate').value;
    }
    if (this.standardForm.get('toDate').value) {
      this.stdFromMaxDate = this.standardForm.get('toDate').value;
    }
  });
}

// threeSixtyFormValidations() {
//   this.threeSixtyForm = this.formBuilderInstance.group({
//     tFromDate: [null, Validators.required],
//     tToDate: [null, Validators.required]
//   });
//   this.valueChangesThreeSixtyForms = this.threeSixtyForm.valueChanges.subscribe((value) => {
//     if (this.threeSixtyForm.get('tFromDate').value) {
//       this.tsToMinDate = this.threeSixtyForm.get('tFromDate').value;
//     }
//     if (this.threeSixtyForm.get('tToDate').value) {
//       this.tsFromMaxDate = this.threeSixtyForm.get('tToDate').value;
//     }
//   });
// }

// On submitting form

// onStandard(values) {
//   let jsonInputObject = {
//     "videoType": 'standard',
//     "fromDate": values.fromDate,
//     "toDate": values.toDate,
//   };
//   const url = AppConstants.apiDomainName + AppConstants.apiStdVideoURL;
//   // console.log("\n--url--", url);
//   this.appServiceInstance.getVideos(url, jsonInputObject).subscribe((data) => {
//     if (data.videos.length > 0) {
//       this.noRecordsFoundFlag = false;
//       this.standardVideoDetails = [];
//       this.standardVideoDetails = data.videos;
//     } else {
//       this.noRecordsFoundFlag = true;
//     }
//   }, Error => {
//     jsonInputObject = null;
//     const err = Error.error;
//     this.errorMessageDisplay(JSON.stringify(err.message));
//   });
// }

// onThreeSixty(values) {
//   let jsonInputObject = {
//     "videoType": '360_videos',
//     "fromDate": values.tFromDate,
//     "toDate": values.tToDate,
//   };
//   const url = AppConstants.apiDomainName + AppConstants.apiThreeSixtyVideoURL;
//   // console.log("\n--url--", url);
//   this.appServiceInstance.getVideos(url, jsonInputObject).subscribe((data) => {
//     if (data.videos) {
//       this.tsNoRecordsFoundFlag = false;
//       this.tsVideoDetails = [];
//       this.tsVideoDetails = data.videos;
//     } else {
//       this.tsNoRecordsFoundFlag = true;
//     }
//   }, Error => {
//     jsonInputObject = null;
//     const err = Error.error;
//     this.errorMessageDisplay(JSON.stringify(err.message));
//   });
// }

onSubmitCommonForm(values) {
  // console.log("\n---values--", values);

  this.loadingSpinnerInstance.show();
  let fromDate = moment(values.fromDate).format('YYYY-MM-DD');
  let toDate = moment(values.toDate).format('YYYY-MM-DD');

  // if (toDate === "Invalid date") {
  //   toDate = moment(this.tomorrowsDate).format('YYYY-MM-DD');
  // }

  let jsonInputObject = {
    "video_type": this.selectedNavPill === 1 ? 'standard' : '360',
    "from_date": fromDate,
    "end_date": toDate,
  };
  let url = AppConstants.apiDomainName;
  url = this.selectedNavPill === 1 ? url + AppConstants.apiStdVideoURL : url + AppConstants.apiThreeSixtyVideoURL;
  // console.log("\n--url--", url, "---jsonInputObject---", jsonInputObject.video_type); //data.videos  data.videos[0].name === 'standard1.mp4'  
  this.appServiceInstance.getVideos(url, jsonInputObject).subscribe((data) => {
    this.loadingSpinnerInstance.hide();

    // console.log("\n--data--", JSON.stringify(data));
    if (data.length > 0) {
      if (jsonInputObject.video_type === 'standard') {
        this.noRecordsFoundFlag = false;
        this.standardVideoDetails = [];
        this.standardVideoDetails = data;
        // console.log("\n---standardVideoDetails-", this.standardVideoDetails);
      } else {
        this.tsNoRecordsFoundFlag = false;
        this.tsVideoDetails = [];
        this.tsVideoDetails = data;
        // console.log("\n--tsVideoDetails--", this.tsVideoDetails);
      }

    } else {
      if (jsonInputObject.video_type === 'standard') {
        this.standardVideoDetails = [];
        this.noRecordsFoundFlag = true;
      } else {
        this.tsVideoDetails = [];
        this.tsNoRecordsFoundFlag = true;
      }
    }
  }, Error => {
    this.loadingSpinnerInstance.hide();
    fromDate = null;
    toDate = null;
    jsonInputObject = null;
    const err = Error.error;
    this.errorMessageDisplay(JSON.stringify(err.message));
  });
}

// Tabs - Accounting software etc...
tabSelection(tabNumber) {
  this.selectedTab = tabNumber;
  if (tabNumber == 2) {
    this.connectWebSocket();
  }
}
// Tabs - standard & 360
navPillSelection(pillNymber) {
  this.selectedNavPill = pillNymber;
}
// New window option
openInBrowser() {
  window.open('http://10.40.10.11/pushstart/', 'Accounting Software', 'toolbar = 0, scrollbars = 1, statusbar = 0, menubar = 0, resizable = 0, height = 800, width = 800');
}

// Tab 2
getFilesStatus(data) {
  // console.log("\n----fileProgressData---Web-socket-response--");

  if (data) {
    if (data.Channel === 'copy') {
      let temp: any;
      this.fileProgressData = [];
      temp = JSON.parse(data.Message);
      // console.log("\n--temp--parse--Message---" + JSON.stringify(temp));
      // console.log("\n---temp.Percent---", temp.Percent, "\n---temp.USBDevice----", temp.USBDevice);
      this.fileProgressData.push({
        'Percent': temp.Percent,
        'USBDevice': temp.USBDevice,
      });
      temp = null;

    } else if (data.Channel === 'success') {
        this.showDownloadNotification(data.Message, 'Success');

    } else if (data.Channel === 'error') {
      this.errorMessageDisplay(data.message);
    }

    // for (let element of this.fileProgressData) {
    //   sessionStorage.setItem(element.DeviceName, element.Pending);
    //   element.DeviceName = element.DeviceName.substring(0, element.DeviceName.length - 1);
    // }
  }

  // const url = AppConstants.apiDomainName + AppConstants.apiFileProgressURL;
  // this.appServiceInstance.getFileProgressStatus(url).subscribe((data) => {
  //   console.log("\n--data---", JSON.stringify(data));

  //   console.log("\n----fileProgressData---", this.fileProgressData);
  //   alert(sessionStorage.getItem('Device 0'));
  //   sessionStorage.setItem('Device 0', '999');
  // });
}

showPreview(index, videoLink, videoName) {
  console.log("\n---Mouse--hover---working--", index);
  let id = this.elementRef.nativeElement.querySelector('#video' + index);
  console.log("\n--SRC--", id);

  // if (this.standardVideoDetails[index]) {
    let updateLink = videoLink; // this.standardVideoDetails[index].link;

    this.tCount = this.tCount + 1;

    const updatedT = this.tCount.toString();
    // console.log ("\n----this.tCount-----", updatedT);

    updateLink = updateLink + videoName + '#t=' + updatedT;
    // console.log("\n-----updateLink------", updateLink);

    this.renderer.setAttribute(this.videoSrc.nativeElement, 'src', updateLink);

    // let updateLoad = $('#video' + index).attr('src', updateLink);

    // this.elementRef.nativeElement.querySelector('#video' + index).load(updateLoad);


    // updateLoad[index].load(document.getElementById('video' + index));
    // updateLoad[index].play();

    // const updateLoad = $('#video' + index);

    // console.log("\n-----ID--SRC--", updateLoad);

    // updateLoad[index].load;
    // updateLoad[index].play;

    // document.getElementById('video' + index).setAttribute('src', updatedLink);
  // }
}
// STD video file
downloadStandardVideoFile(index) {
  console.log("\n---downloadStandardVideoFile---call---");

  let url = AppConstants.apiDomainName + AppConstants.apiVideoDownload;
  let fileName;
  let videoLink;
  let inputObject;

  if (this.standardVideoDetails[index]) {
    fileName = this.standardVideoDetails[index].video_name;
    videoLink = this.standardVideoDetails[index].download_path;
  }

  url = url;  // + '/' + fileName;

  // videoLink = videoLink.substring(0, videoLink.length - 1);

  inputObject = {
    "video_link" : videoLink,
    "video_name": fileName
  };
  console.log("\n---donwload file with url--&--inputObject--", url, "\n--inputObject--", JSON.stringify(inputObject));

  this.appServiceInstance.videoFileDownload(url, inputObject).subscribe((data) => {

    const BLOB = new Blob([data], {type: 'video/mp4'});
    const FILE = new File([BLOB], fileName.toString(), {type: 'video/mp4'});
    saveAs(FILE);

    this.showDownloadNotification(fileName, "File downloaded");

  }, Error => {
    fileName = null;
    const err = Error.error;
    this.errorMessageDisplay(JSON.stringify(err.message));
  });
}

// 360 video file
download360VideoFile(index) {
  console.log("\n---download360VideoFile---call---");
  let url = AppConstants.apiDomainName + AppConstants.apiVideoDownload;
  let fileName;
  let videoLink;
  let inputObject;

  if (this.tsVideoDetails[index]) {
    videoLink = this.tsVideoDetails[index].download_path;
    fileName = this.tsVideoDetails[index].video_name;
  }

  url = url; // + '/' + fileName;

  // videoLink = videoLink.substring(0, videoLink.length - 1);
  // let strSplit = videoLink.split(':8000');

  // videoLink = strSplit[1];
  // console.log("\n---videoLink---", videoLink);

  inputObject = {
    "video_link" : videoLink,
    "video_name": fileName
  };
  console.log("\n---donwload file with url--&--filename--", url);

  this.appServiceInstance.videoFileDownload(url, inputObject).subscribe((data) => {

    const BLOB = new Blob([data], {type: 'video/mp4'});
    const FILE = new File([BLOB], fileName.toString(), {type: 'video/mp4'});
    saveAs(FILE);

    this.showDownloadNotification(fileName, "File downloaded");

  }, Error => {
    fileName = null;
    const err = Error.error;
    this.errorMessageDisplay(JSON.stringify(err.message));
  });

}

ngOnDestroy() {
  this.valueChangesStdForms.unsubscribe();
  this.websocketServiceInstance.myWebSocket.complete();
  // this.valueChangesThreeSixtyForms.unsubscribe();
  // clearInterval();
}

}

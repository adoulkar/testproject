import { Injectable } from '@angular/core';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  myWebSocket: WebSocketSubject<any>;

  constructor() { }

  webSocketConnection(url) {
    // 'ws://localhost:8000'
   this.myWebSocket = webSocket(url);
  }

}

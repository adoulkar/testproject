import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getFileProgressStatus(url): Observable<any> {
    return this.http.get(url).pipe(map(res => res));
  }

  getVideos(url, data): Observable<any> {
    return this.http.post(url, data).pipe(map(res => res));
  }

  videoFileDownload(url, data): Observable<any> {
    // let url = "http://localhost:7000/fs/downladExcel/"+filename;
    // return this._http.get(url, { responseType: 'blob'}).pipe(map(res=>res));

    return this.http.post(url, data, {responseType: 'blob'}).pipe(map(res => res));
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.scss']
})
export class VideoDetailsComponent implements OnInit {
tableData: any = [];
  constructor() { }

  ngOnInit(): void {
    this.tableData = [];
    for (let index = 1; index < 100; index++) {
      this.tableData.push({
        'index': index,
        'videoName': 'Ferrari_Video - ' + index,
        'path': 'C:\/Program Files\/Common Files',
      });
    }
  }

}

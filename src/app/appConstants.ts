export class AppConstants {
   // port: 8080
   public static apiDomainName = 'http://localhost:8000';

   // Progress of storing video '/api/cards/status'
   public static apiFileProgressURL = '/api/progress';
   // Standard Video URL ?type=standard
   public static apiStdVideoURL = '/api/videos';
   // Three-Sixty Video URL ?type=360
   public static apiThreeSixtyVideoURL = '/api/videos';
   // Video download URL
   public static apiVideoDownload = '/api/download';
}

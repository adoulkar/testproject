import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
    // allowedOrigin: any = ['http://localhost:4200', 'http://localhost:8080'];
    constructor() {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //  request = request.clone({
        //     headers: request.headers.set('Access-Control-Allow-Origin', '*')
        //     .append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        //     .append('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
        // });
        return next.handle(request);
    }
}

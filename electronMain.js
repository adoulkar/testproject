const { app, BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");
// const { checkServer } = require("./serverInfo");

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1400,
    height: 600,
    backgroundColor: "#ffffff",
    icon: `file://${__dirname}/dist/assets/images/icons/mac/logo.icns`,
  });
  // win.loadURL('http://localhost:4200');
  // win.webContents.openDevTools();
  win.loadURL(
    url.format({
      fullscreen: true,
      pathname: path.join(__dirname, "./dist/AVFS-Prototype/index.html"),
      protocol: "file:",
      slashes: true,
    })
  );

  win.on("closed", function () {
    win = null;
  });
  // checkServer(win);
}

app.on("ready", createWindow);

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  if (win === null) {
    createWindow();
  }
});
